import pytest

ENDPOINT = '/api/contracts/'

from utils.subgroup_util import Subgroup

@pytest.mark.django_db
class TestSubgroupUtil:
    def setup_method(self):
        self.supply_voltage_negative = -1
        self.supply_voltage_70 = 70
        self.supply_voltage_69 = 69
        self.supply_voltage_0 = 0

    def test_get_subgroup_throws_exception_for_negative_voltage(self):
        with pytest.raises(Exception) as e:
            Subgroup.get_subgroup(self.supply_voltage_negative)
        assert 'Subgroup not found' in str(e.value)

    def test_get_subgroup_throws_exception_for_voltage_70(self):
        with pytest.raises(Exception) as e:
            Subgroup.get_subgroup(self.supply_voltage_70)
        assert 'Subgroup not found' in str(e.value)

    def test_get_subgroup_is_A3(self):
        assert Subgroup.get_subgroup(self.supply_voltage_69) == Subgroup.A3

    def test_get_subgroup_is_AS(self):
        assert Subgroup.get_subgroup(self.supply_voltage_0) == Subgroup.AS

    def test_get_all_subgroups(self):
        expected_subgroups = [
            {"name": Subgroup.AS, "min": 0, "max": 2.3},
            {"name": Subgroup.A4, "min": 2.3, "max": 25},
            {"name": Subgroup.A3A, "min": 30, "max": 44},
            {"name": Subgroup.A3, "min": 69, "max": 69},
            {"name": Subgroup.A2, "min": 88, "max": 138},
            {"name": Subgroup.A1, "min": 230, "max": None},
        ]
        assert Subgroup.get_all_subgroups() == expected_subgroups

